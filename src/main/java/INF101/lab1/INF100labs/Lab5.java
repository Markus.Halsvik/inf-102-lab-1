package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        if (list.isEmpty()) {
            return list;
        }
        for (int i = 0; i < list.size(); i++) {
            list.set(i, list.get(i) * 2);
        }
        return list;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        if (list.isEmpty()) {
            return list;
        }
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (list.get(i) == 3) {
                list.remove(i);
                i--;
                size--;
            }
        }
        return list;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        if (list.isEmpty()) {
            return list;
        }
        ArrayList<Integer> unique = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Integer listInt = list.get(i);
            if (!unique.contains(listInt)) {
                unique.add(listInt);
            }
        }
        return unique;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    }

}