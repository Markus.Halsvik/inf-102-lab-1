package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        List <String> ws = new ArrayList<>();
        ws.add(word1);
        ws.add(word2);
        ws.add(word3);
        int max = ws.get(0).length();
        for (int i = 1; i < 3; i++) {
            int l = ws.get(i).length();
            if (l > max) {
            max = l;
            }
        }
        for (int i = 0; i < 3; i++) {
            String word = ws.get(i);
            int len = word.length();
            if (len == max) {
                System.out.println(word);
            }
        }
    }

    public static boolean isLeapYear(int year) {
        if (year % 400 == 0) {
            return true;
        }
        if (year % 100 == 0) {
            return false;
        }
        if (year % 4 == 0) {
            return true;
        }
        return false;
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0 && num % 2 == 0) {
            return true;
        }
        return false;
    }

}
