package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void multiplesOfSevenUpTo(int n) {
        int m = n / 7 + 1;
        for (int i = 1; i < m; i++) {
            System.out.println(i * 7);
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i < n + 1; i++) {
            System.out.print(i + ":");
            for (int j = 1; j < n + 1; j++) {
                System.out.print(" " + (i * j));
            }
            System.out.println();
        }
    }

    public static int crossSum(int num) {
        String str = String.valueOf(num);
        int len = str.length();
        int sum = 0;
        for (int i = 0; i < len; i++) {
            sum += Character.getNumericValue(str.charAt(i));
        }
        return sum;
    }

}