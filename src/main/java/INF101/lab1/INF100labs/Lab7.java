package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        if (grid.isEmpty()) {
            return;
        }
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        int rowSum = 0;
        int colSum = 0;
        for (int i : grid.get(0)) {
            rowSum += i;
        }
        for (ArrayList <Integer> row : grid) {
            colSum += row.get(0);
        }
        for (int i = 1; i < grid.size(); i++) {
            int sum = 0;
            for (int j : grid.get(i)) {
                sum += j;
            }
            if (sum != rowSum) {
                return false;
            }
        }
        for (int i = 1; i < grid.get(0).size(); i++) {
            int sum = 0;
            for (ArrayList <Integer> row : grid) {
                sum += row.get(i);
            }
            if (sum != colSum) {
                return false;
            }
        }
        return true;
    }
}