package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;

import javax.sound.midi.SysexMessage;


public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        String userInput = readInput("Rock, paper or scissors?");
        if (!rpsChoices.contains(userInput)) {
            System.out.println("Try again, that is not a valid choice.");
            run();
        }
        Random rand = new Random();
        int compPick = rand.nextInt(3);
        int userPick = 0;
        for (int i = 0; i < 3; i++) {
            if (userInput.equals(rpsChoices.get(i))) {
                userPick = i;
            }
        }
        System.out.println("Computer chose: " + rpsChoices.get(compPick));
        if (userPick == compPick) { // Same object
            System.out.println("Tie!!");
            roundCounter++;
            run();
        }
        if (userPick > compPick && userPick - compPick == 1) { // Works due to format of rpsChoices
            System.out.println("You win!");
            humanScore++;
            roundCounter++;
            run();
        }
        if (userPick < compPick && compPick - userPick == 2) { // Special case for picking rock vs scissors
            System.out.println("You win!");
            humanScore++;
            roundCounter++;
            run();
        }
        System.out.println("You lose... :("); // Any other case is a loss
        computerScore++;
        roundCounter++;
        run();
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}